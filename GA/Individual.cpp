/* 
 * File:   Individual.cpp
 * Author: Walisson Cardoso Gomes
 * 
 * Created on January 29, 2016, 10:31 PM
 */

#include <stdlib.h>

#include "Individual.h"

Individual::Individual()
: cromossome(sizeCromossome), cromSize(sizeCromossome), fitness(0.0) {
    for(int i = 0; i < sizeCromossome; i++) {
        cromossome[i] = 0;
    }
}

Individual::Individual(std::vector<typeCromossome> crm)
: cromossome(sizeCromossome), cromSize(sizeCromossome), fitness(0.0) {
    for(int i = 0; i < sizeCromossome; i++)
        cromossome[i] = crm[i];
}

Individual::Individual(const Individual &orig)
: cromSize(sizeCromossome) {
    
    fitness = orig.fitness;
    cromossome = orig.cromossome;
}

void Individual::initCromossome() {
    
        switch(nameType){
        case 'b':
            for(int i = 0; i < cromSize; i++)
                cromossome[i] = mt.genrand_int31() % 2;
            break;
        case 'i':
            for(int i = 0; i < cromSize; i++)
                cromossome[i] = (mt.genrand_int31() % (maxValGene-minValGene+1) ) + minValGene;
            break;
        case 'f':
            for(int i = 0; i < cromSize; i++)
                cromossome[i] = (mt.random() * (maxValGene-minValGene)) + minValGene;
            break;
        default:
            std::cout << "An inexistent type of codification was selected. Aborting!" << std::endl;
            exit(1);
    }
}

void Individual::setFitness(const double fit) {
    fitness = fit;
}

std::vector<typeCromossome> Individual::getGenotype() {
    return cromossome;
}

double Individual::getFitness() {
    return fitness;
}

void Individual::print() {
    
    if(nameType == 'b' || nameType == 'i')
        for(int i = 0; i < sizeCromossome; i++)
            std::cout << cromossome[i] << ' ';
    else
        for(int i = 0; i < sizeCromossome; i++)
            std::cout << std::setprecision(2) << cromossome[i] << ' ';
}

Individual& Individual::operator =(const Individual &other) {
    fitness = other.fitness;   
    cromossome = other.cromossome;
    return *this;
}

Individual::~Individual() {
}

