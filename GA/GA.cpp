
#include <stdlib.h>

#include "GA.h"


GA::GA()
: maxGenerations(numberGen) {
    
}

void GA::initGA() {    
    pop.initPopulation();
}

void GA::run() {
    
    pop.evaluatePopulation();
    for(int i = 0; i < maxGenerations; i++) {
        pop.applyOperators();
        pop.nextGeneration();
        pop.evaluatePopulation();
    }
}
