/* 
 * File:   GA.h
 * Author: Walisson Cardoso Gomes
 *
 * Created on January 29, 2016, 10:23 PM
 */

#ifndef GA_H
#define	GA_H

#include "Population.h"

//mt.init_genrand(time(NULL));

class GA {
public:
    GA();
    void initGA();
    void run();
private:
    const int maxGenerations;
    Population pop;
};


#endif	/* GA_H */

