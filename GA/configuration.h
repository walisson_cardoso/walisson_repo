/* 
 * File:   configuration.h
 * Author: Walisson Cardoso Gomes
 *
 * Created on January 30, 2016, 8:04 AM
 */

#ifndef CONFIGURATION_H
#define	CONFIGURATION_H

#include <iostream>
#include <iomanip>
#include "mt.h"
extern MersenneTwister mt;


#define numberGen      100
#define sizePopulation 10
#define sizeCromossome 10
#define applyElitism   true

/* The name of type of cromossome can be one of the following:
    - 'b' = binary codification
    - 'i' = integer codification
    - 'f' = float point codification
 * The type can be
    - short int - for binary
    - int       - for integer
    - float     - and for float point codification.
 * Every type of codification could be soulved by using a double number, but
 * we try to save memory doing so.
*/
#define nameType       'f'
#define typeCromossome float
#define minValGene     0        //General Minimum value to a gene
#define maxValGene     3        //General Maximum value to a gene

/* The type of selection can be one of the following:
    - 'r' = roulette selection
    - 'x' = ring selection
*/
#define typeSelec      'x'

/* The type of crossover can be one of the following:
    - 'o' = One point crossover
    - 't' = two point crossover
    - 'u' = uniform crossover
    - 'a' = arithmetic crossover. Exclusive to real problems
    - 'd' = order crossover. Exclusive to ordering problems.
*/
#define pProb          0.8
#define typeCros       'a'
#define ringSize       4

/* The type of mutation can be one of the following:
    - 'f' = flip mutation. Dedicated to binary codification
    - 'i' = integer mutation. It changes the number to a random one
    - 'g' = gaussian mutation. It changes the number to a value following the gaussian distribution*
    - 'd' = order mutation. Mutation dedicated to order problems
*/
#define pMut           0.01
#define typeMut        'g'

#endif	/* CONFIGURATION_H */

