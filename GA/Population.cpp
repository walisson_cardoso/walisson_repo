/* 
 * File:   Population.cpp
 * Author: Walisson Cardoso Gomes
 * 
 * Created on January 29, 2016, 10:28 PM
 */

#include <stdlib.h>
#include <iostream>

#include "Population.h"
#include "mt.h"

Population::Population()
:popSize((sizePopulation / 2 ) * 2), crossoverProb(pProb), mutationProb(pMut) {
    
}

void Population::initPopulation() {
    for(int i = 0; i < popSize; i++){
        inds[i].initCromossome();
    }
    bestIndividual.setFitness(0.0);
    worstIndividual.setFitness(1.0);
    sumFitness = 0.0;
}

void Population::nextGeneration() {
    
    for(int i = 0; i < popSize; i++) {
        inds[i] = nextInds[i];
    }
    if(applyElitism){
        inds[0] = bestIndividual;
    }
}

void Population::evaluatePopulation() {
    
    short int target[sizeCromossome] = {1,0,1,1,0,1,1,1,1,0};
    double sumFit = 0.0;
    
    for(int i = 0; i < popSize; i++){
        std::vector<typeCromossome> cromossome = inds[i].getGenotype();
        double fitness = 0.0;
        
        for(int j = 0; j < sizeCromossome; j++){
            if(target[j] == cromossome[j])
                fitness += 0.1;
        }
        inds[i].setFitness(fitness);
        if(fitness > bestIndividual.getFitness())
            bestIndividual = inds[i];
        else if(fitness < worstIndividual.getFitness())
            worstIndividual = inds[i];
        
        sumFit += fitness;
    }
                
    sumFitness = sumFit;
    
    printPopulation();
    
    std::cout << "\n--- info: " << '\n' << worstIndividual.getFitness() << "\t" << bestIndividual.getFitness() << "\t" << sumFitness / popSize << std::endl;
}

void Population::applyOperators() {
    
    switch(typeSelec){
        case 'r':
            applyRoullette();
            break;
        case 'x':
            applyRingSelection(ringSize);
            break;
        default:
            std::cout << "An inexistent type of crossover was selected. Aborting!" << std::endl;
            exit(1);
    }
    
    switch(typeCros){
        case 'o':
            applyOnePointCrossover();
            break;
        case 't':
            applyTwoPointCrossover();
            break;
        case 'u':
            appplyUniformCrossover();
            break;
        case 'a':
            applyArithmeticCrossover();
            break;
        case 'd':
            applyOrderCrossover();
            break;
        default:
            std::cout << "An inexistent type of crossover was selected. Aborting!" << std::endl;
            exit(1);
    }
    
    switch(typeMut){
        case 'f':
            applyFlipMutation();
            break;
        case 'i':
            applyIntMut();
            break;
        case 'g':
            applyGauMut();
            break;
        case 'd':
            applyOrderMut();
            break;
        default:
            std::cout << "An inexistent type of mutation was selected. Aborting!" << std::endl;
            exit(1);
    }
}


Individual Population::getBestIndividual() {
    return bestIndividual;
}

Individual Population::getWorstIndividual() {
    return worstIndividual;
}

double Population::getBestFitness() {
    return getBestIndividual().getFitness();
}

double Population::getWorstFitness() {
    return getWorstIndividual().getFitness();
}

double Population::getSumFitness() {
    return sumFitness;
}


Population::~Population() {
    
}

void Population::applyRoullette() {
    for(int i = 0; i < popSize; i++) {
        double selected = sumFitness * mt.random();
        double wheel = 0.0;
        int index = 0;
        for(int j = 0; j < popSize; j++){
            wheel += inds[j].getFitness();
            if(wheel >= selected){
                index = j;
                break;
            }
        }
        selectedInds[i] = inds[index];
    }
}

void Population::applyRingSelection(const int size) {
    for(int i = 0; i < popSize; i++) {
        int index = mt.genrand_int31() % popSize;
        for(int j = 0; j < size; j++){
            int vs = mt.genrand_int31() % popSize;
            if(inds[index].getFitness() < inds[vs].getFitness()){
                index = vs;
            }
        }
        selectedInds[i] = inds[index];
    }
}

void Population::applyOnePointCrossover() {
    
    for(int i = 0; i < popSize - 1; i += 2){
        if(mt.random() < crossoverProb){
            int cutPoint = (mt.genrand_int31() % (sizeCromossome - 1));
            std::vector<typeCromossome> cromossome1 = selectedInds[i].getGenotype();
            std::vector<typeCromossome> cromossome2 = selectedInds[i+1].getGenotype();

            for(int j = 0; j <= cutPoint; j++){
                typeCromossome aux = cromossome1[j];
                cromossome1[j] = cromossome2[j];
                cromossome2[j] = aux;
            }
            Individual son1(cromossome1);
            Individual son2(cromossome2);

            nextInds[i] = son1;
            nextInds[i+1] = son2;
        }else{
            nextInds[i] = selectedInds[i];
            nextInds[i+1] = selectedInds[i+1];
        }
    }
}

void Population::applyTwoPointCrossover() {
    
    for(int i = 0; i < popSize - 1; i += 2){
        if(mt.random() < crossoverProb){
            int cutPoint1 = (mt.genrand_int31() % (sizeCromossome - 1));
            int cutPoint2 = ((mt.genrand_int31() % (sizeCromossome - 1))+1);
            if(cutPoint1 > cutPoint2){
                int aux = cutPoint1;
                cutPoint1 = cutPoint2;
                cutPoint2 = aux;
            }

            std::vector<typeCromossome> cromossome1 = selectedInds[i].getGenotype();
            std::vector<typeCromossome> cromossome2 = selectedInds[i+1].getGenotype();

            for(int j = cutPoint1; j <= cutPoint2; j++){
                typeCromossome aux = cromossome1[j];
                cromossome1[j] = cromossome2[j];
                cromossome2[j] = aux;
            }
            Individual son1(cromossome1);
            Individual son2(cromossome2);

            nextInds[i] = son1;
            nextInds[i+1] = son2;
        }else{
            nextInds[i] = selectedInds[i];
            nextInds[i+1] = selectedInds[i+1];
        }
    }
}

void Population::appplyUniformCrossover() {
    for(int i = 0; i < popSize - 1; i += 2){
        if(mt.random() < crossoverProb){
            std::vector<typeCromossome> cromossome1 = selectedInds[i].getGenotype();
            std::vector<typeCromossome> cromossome2 = selectedInds[i+1].getGenotype();

            for(int j = 0; j < sizeCromossome; j++){
                if(mt.random() >= 0.5){
                    typeCromossome aux = cromossome1[j];
                    cromossome1[j] = cromossome2[j];
                    cromossome2[j] = aux;
                }
            }
            Individual son1(cromossome1);
            Individual son2(cromossome2);

            nextInds[i] = son1;
            nextInds[i+1] = son2;
        }else{
            nextInds[i] = selectedInds[i];
            nextInds[i+1] = selectedInds[i+1];
        }
    }
}

void Population::applyArithmeticCrossover() {
    for(int i = 0; i < popSize - 1; i += 2){
        if(mt.random() < crossoverProb){
            std::vector<typeCromossome> cromossome1 = selectedInds[i].getGenotype();
            std::vector<typeCromossome> cromossome2 = selectedInds[i+1].getGenotype();

            double alpha = mt.random();
            for(int j = 0; j < sizeCromossome; j++){
                double aux1 = alpha * cromossome1[j] + (1-alpha) * cromossome2[j];
                double aux2 = alpha * cromossome2[j] + (1-alpha) * cromossome1[j];
                cromossome1[j] = aux1;
                cromossome2[j] = aux2;
            }
            Individual son1(cromossome1);
            Individual son2(cromossome2);

            nextInds[i] = son1;
            nextInds[i+1] = son2;
        }else{
            nextInds[i] = selectedInds[i];
            nextInds[i+1] = selectedInds[i+1];
        }
    }
}

void Population::applyOrderCrossover() {

}

void Population::applyFlipMutation() {
    for(int i = 0; i < popSize; i++){
        std::vector<typeCromossome> cromossome = nextInds[i].getGenotype();
        for(int j = 0; j < sizeCromossome; j++){
            if(mt.random() < mutationProb){
                cromossome[j] = 1 - cromossome[j];
            }
        }
        Individual tempInd(cromossome);
        nextInds[i] = tempInd;
    }
}

void Population::applyIntMut() {
    for(int i = 0; i < popSize; i++){
        std::vector<typeCromossome> cromossome = nextInds[i].getGenotype();
        for(int j = 0; j < sizeCromossome; j++){
            if(mt.random() < mutationProb){
                cromossome[j] = (mt.genrand_int31() % (maxValGene-minValGene+1)+minValGene);
            }
        }
        Individual tempInd(cromossome);
        nextInds[i] = tempInd;
    }
}

void Population::applyGauMut() {
    for(int i = 0; i < popSize; i++){
        std::vector<typeCromossome> cromossome = nextInds[i].getGenotype();
        for(int j = 0; j < sizeCromossome; j++){
            if(mt.random() < mutationProb){
                double gauss = (mt.nextGaussian(0,1))/2+1;
                cromossome[j] = cromossome[j] * gauss;
                if(cromossome[j] < minValGene)
                    cromossome[j] = minValGene;
                if(cromossome[j] > maxValGene)
                    cromossome[j] = maxValGene;
            }
        }
        Individual tempInd(cromossome);
        nextInds[i] = tempInd;
    }
}

void Population::applyOrderMut() {

}

void Population::printPopulation() {
    std::cout << "\n\n";
    for(int i = 0; i < popSize; i++){
        inds[i].print();
        std::cout << "\tFit: " << inds[i].getFitness() << '\n';
    }
}

