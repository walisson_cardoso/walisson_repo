/* 
 * File:   Individual.h
 * Author: gomes
 *
 * Created on January 29, 2016, 10:31 PM
 */

#ifndef INDIVIDUAL_H
#define	INDIVIDUAL_H

#include "configuration.h"
#include <ctime>
#include <cmath>
#include <vector>

class Individual {
public:
    Individual();
    Individual(std::vector<typeCromossome> crm);
    Individual(const Individual &orig);
    void initCromossome();
    void setFitness(const double);
    std::vector<typeCromossome> getGenotype();
    double getFitness();
    void print();
    Individual& operator=(const Individual &other);
    ~Individual();
private:
    double fitness;
    const int cromSize;
    std::vector<typeCromossome> cromossome;
};

#endif	/* INDIVIDUAL_H */

