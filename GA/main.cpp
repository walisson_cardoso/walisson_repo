/* 
 * File:   main.cpp
 * Author: Walisson Cardoso Gomes
 *
 * Created on January 29, 2016, 10:16 PM
 */

#include <cstdlib>
#include <cstdlib>
#include <ctime>

#include "GA.h"

MersenneTwister mt;

int main(int argc, char** argv) {
    
    mt.init_genrand(time(NULL));
    
    GA ga;
    ga.initGA();
    ga.run();
    
    return 0;
}

