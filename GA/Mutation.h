/* 
 * File:   Mutation.h
 * Author: gomes
 *
 * Created on January 29, 2016, 10:38 PM
 */

#ifndef MUTATION_H
#define	MUTATION_H

#include "Individual.h"


class Mutation {
public:
    Mutation();
    Mutation(const Mutation& orig);
    Individual applyMutation(Individual ind);
    ~Mutation();
private:
    double mutationProb;
};

#endif	/* MUTATION_H */

