/* 
 * File:   Population.h
 * Author: Walisson Cardoso Gomes
 *
 * Created on January 29, 2016, 10:28 PM
 */

#ifndef POPULATION_H
#define	POPULATION_H

#include "Individual.h"


class Population {
public:
    Population();
    void initPopulation();
    void nextGeneration();
    void evaluatePopulation();
    void applyOperators();
    Individual getBestIndividual();
    Individual getWorstIndividual();
    double getBestFitness();
    double getWorstFitness();
    double getSumFitness();
    ~Population();
protected:
    void applyRoullette();
    void applyRingSelection(const int size);
    void applyOnePointCrossover();
    void applyTwoPointCrossover();
    void appplyUniformCrossover();
    void applyArithmeticCrossover();
    void applyOrderCrossover();
    void applyFlipMutation();
    void applyIntMut();
    void applyGauMut();
    void applyOrderMut();
    void printPopulation();
private:
    const int popSize;
    double crossoverProb;
    double mutationProb;
    double sumFitness;
    Individual bestIndividual;
    Individual worstIndividual;
    Individual inds[sizePopulation];
    Individual selectedInds[sizePopulation];
    Individual nextInds[sizePopulation];
};

#endif	/* POPULATION_H */

